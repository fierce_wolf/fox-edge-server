/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.domain.constant;

public class RestFulManagerVOConstant {
    public static final String restful_manager = "restful_manager";
    public static final String uri_device_value = "/manager/system/device/value/entities";
}
