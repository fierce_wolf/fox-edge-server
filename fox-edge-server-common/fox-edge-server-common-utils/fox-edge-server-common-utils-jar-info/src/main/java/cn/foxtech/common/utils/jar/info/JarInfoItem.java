/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.utils.jar.info;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JarInfoItem {
    private String groupId;
    private String artifactId;
    private String version;
}
