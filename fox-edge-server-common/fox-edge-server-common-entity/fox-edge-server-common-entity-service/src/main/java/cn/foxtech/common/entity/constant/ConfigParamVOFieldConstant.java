/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.entity.constant;

public class ConfigParamVOFieldConstant {
    public static final String field_list = "list";
    public static final String field_password = "password";
}
