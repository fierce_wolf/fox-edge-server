/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.entity.constant;

public class EntityOptionConstant {
    /**
     * optionEntity的数值：表名称和字段名称
     */
    public static final String field_option_table_name = "tableName";
    public static final String field_option_field_name = "fieldName";
}
