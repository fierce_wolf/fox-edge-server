/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 *
 *     This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * --------------------------------------------------------------------------- */
 
package cn.foxtech.common.entity.constant;

public class DeviceModelVOFieldConstant extends BaseVOFieldConstant {
    public static final String field_model_name = "modelName";
    public static final String field_device_type = "deviceType";
    public static final String field_manufacturer = "manufacturer";
    public static final String field_model_param = "modelParam";
    public static final String field_extend_param = "extendParam";

    public static final String field_comp_id = "compId";

    public static final String field_model_id = "modelId";
    public static final String field_group_name = "groupName";
}
