/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.entity.constant;

public class UserPermissionVOFieldConstant extends BaseVOFieldConstant {
    /**
     * 字段名称
     */
    public static final String field_name = "name";
    public static final String field_permission = "permission";
}
