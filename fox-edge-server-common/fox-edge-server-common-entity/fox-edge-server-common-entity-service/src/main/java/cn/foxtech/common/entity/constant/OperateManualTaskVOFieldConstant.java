/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.entity.constant;

public class OperateManualTaskVOFieldConstant extends BaseVOFieldConstant {
    public static final String field_task_name = "taskName";
    public static final String field_device_name = "deviceName";
    public static final String field_device_type = "deviceType";
    public static final String field_manufacturer = "manufacturer";
    public static final String field_task_param = "taskParam";
}
