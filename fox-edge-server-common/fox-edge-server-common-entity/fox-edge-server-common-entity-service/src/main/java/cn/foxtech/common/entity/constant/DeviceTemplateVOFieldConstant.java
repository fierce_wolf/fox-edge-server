/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 *
 *     This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.entity.constant;

public class DeviceTemplateVOFieldConstant extends BaseVOFieldConstant {
    public static final String field_template_name = "templateName";
    public static final String field_device_type = "deviceType";
    public static final String field_manufacturer = "manufacturer";
    public static final String field_subset_name = "subsetName";
    public static final String field_template_param = "templateParam";
    public static final String field_template_type = "templateType";
    public static final String field_extend_param = "extendParam";

    public static final String field_comp_id = "compId";

    public static final String field_template_id = "templateId";
    public static final String field_group_name = "groupName";

    public static final String value_operate_param = "operate-param";
    public static final String value_device_param = "device-param";
    public static final String value_channel_param = "channel-param";

}
