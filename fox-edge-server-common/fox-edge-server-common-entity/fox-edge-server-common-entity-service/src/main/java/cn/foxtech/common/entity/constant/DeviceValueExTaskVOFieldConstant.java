/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.common.entity.constant;

public class DeviceValueExTaskVOFieldConstant extends BaseVOFieldConstant {
    public static final String field_task_name = "taskName";
    public static final String field_task_param = "taskParam";

    public static final String field_script = "script";
}
