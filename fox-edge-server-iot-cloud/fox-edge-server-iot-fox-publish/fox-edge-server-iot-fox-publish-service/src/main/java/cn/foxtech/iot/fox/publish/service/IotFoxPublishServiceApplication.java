/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.iot.fox.publish.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotFoxPublishServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(IotFoxPublishServiceApplication.class, args);
    }

}
