/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2020. All rights reserved.
 *
 *     This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * --------------------------------------------------------------------------- */

/**
 * 用户自定义的函数：
 * 注意，保持设备类型中的唯一性，设备类型级别的Script引擎，会自动装载include类型的各个操作方法
 * 那么，其他真正的设备操作方法，就就可以引用它了
 * /



/**
 * 用户自定义的函数1：注意，保持设备类型中的全局唯一性
 */
/**
function func1(message)
{
}
 */


/**
 * 用户自定义的函数2：注意，保持设备类型中的全局唯一性
 */
/**
 function func2(message)
 {
}
 */