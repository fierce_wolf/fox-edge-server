/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.kernel.system.repository.constants;

public class RepoConfigConstant {
    public static final String field_config_name = "repositoryConfig";
    public static final String field_config_host = "host";
    public static final String field_config_file = "file";
    public static final String field_config_username = "username";
    public static final String field_config_password = "password";
    public static final String field_config_lockdown = "lockdown";
}
