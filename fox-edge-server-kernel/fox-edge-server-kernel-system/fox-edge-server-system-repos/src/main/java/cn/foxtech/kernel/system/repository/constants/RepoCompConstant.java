/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.kernel.system.repository.constants;

public class RepoCompConstant {
    public static final String repository_type_decoder = "decoder";
    public static final String repository_type_service = "service";

    public static final String repository_type_webpack = "webpack";


    public static final String field_model_name = "modelName";
    public static final String field_model_version = "modelVersion";

    public static final String field_model_type = "modelType";
    public static final String field_description = "description";
    public static final String field_version = "version";
    public static final String field_stage = "stage";
    public static final String field_versions = "versions";
    public static final String field_last_version = "lastVersion";
    public static final String field_used_version = "usedVersion";

    public static final String field_component = "component";
    public static final String field_work_mode = "workMode";
    public static final String field_arch = "arch";
    public static final String field_status = "status";

    public static final String field_local_md5 = "localMd5";
    public static final String field_md5 = "md5";
    public static final String field_path_name = "pathName";
    public static final String field_file_name = "fileName";

    public static final String field_keyword = "keyword";

    public static final String field_commit_key = "commitKey";

    public static final String field_manufacturer = "manufacturer";
    public static final String field_device_type = "deviceType";

    public static final String value_default_manufacturer = "Fox-Edge";
    public static final String value_default_device_type = "public";


    public static final String field_value_model_version_default = "v1";
}
