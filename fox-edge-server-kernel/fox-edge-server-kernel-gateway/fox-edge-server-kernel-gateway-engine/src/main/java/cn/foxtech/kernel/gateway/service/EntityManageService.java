/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.kernel.gateway.service;

import cn.foxtech.common.entity.entity.UserEntity;
import cn.foxtech.common.entity.entity.UserMenuEntity;
import cn.foxtech.common.entity.entity.UserPermissionEntity;
import cn.foxtech.common.entity.entity.UserRoleEntity;
import cn.foxtech.common.entity.manager.EntityServiceManager;
import cn.foxtech.utils.common.utils.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * 数据实体业务
 */
@Component
public class EntityManageService extends EntityServiceManager {
    @Autowired
    private RedisService redisService;

    public void instance() {
        this.instance(this.redisService);

        Set<String> reader = this.entityRedisComponent.getReader();

        // 注册redis读数据
        reader.add(UserEntity.class.getSimpleName());
        reader.add(UserRoleEntity.class.getSimpleName());
        reader.add(UserMenuEntity.class.getSimpleName());
        reader.add(UserPermissionEntity.class.getSimpleName());
    }
}
