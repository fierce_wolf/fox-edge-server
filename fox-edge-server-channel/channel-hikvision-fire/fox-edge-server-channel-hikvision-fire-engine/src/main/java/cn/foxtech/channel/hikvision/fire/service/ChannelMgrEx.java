/* ----------------------------------------------------------------------------
 * Copyright (c) Guangzhou Fox-Tech Co., Ltd. 2020-2024. All rights reserved.
 * --------------------------------------------------------------------------- */

package cn.foxtech.channel.hikvision.fire.service;

import cn.foxtech.channel.socket.core.service.ChannelManager;
import org.springframework.stereotype.Component;

@Component
public class ChannelMgrEx extends ChannelManager {
}
